package printer;

public class DerivedPrinter extends Printer{

    //метод переопределён, чтобы выводить текст красным цветом
    @Override
    protected void print(String value){
        System.out.println(ANSI_RED + value + ANSI_RESET);
    }
}
