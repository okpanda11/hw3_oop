package printer;

public class Main extends Printer{
    public static void main(String[] args) {

        // пишем красным через переопределённый метод в дочернем классе
        Printer derivedPrinter = new DerivedPrinter();
        derivedPrinter.print("derivedPrinter text");

        // пишем красным через константы, унаследованные из базового класса
        Printer basePrinter = new Printer();
        basePrinter.print(ANSI_RED+"basePrinter text"+ANSI_RESET);

    }


}
