package task2;

public class BadPupil extends Pupil{
    @Override
    protected void study() {
        System.out.println("BAD pupil study");
    }

    @Override
    protected void read() {
        System.out.println("BAD pupil read");
    }

    @Override
    protected void write() {
        System.out.println("BAD pupil write");
    }

    @Override
    protected void relax() {
        System.out.println("BAD pupil relax");
    }
}
