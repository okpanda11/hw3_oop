package task2;

import java.util.ArrayList;

public class ClassRoom {

    // constructor, принимает любое кол-во параметров типа Pupil
    protected ClassRoom(Pupil... concretePupil) {
    }

    public static void main(String[] args) {

        // создаём учеников
        Pupil goodBoy1 = new GoodPupil();
        Pupil goodBoy2 = new GoodPupil();
        Pupil badBoy = new BadPupil();
        Pupil excellentBoy = new ExcellentPupil();

        // массив типа данных Pupil с объектами
        Pupil[] myClass = {badBoy, excellentBoy, goodBoy1, goodBoy2};

        // в цикле вызываем у объектов их методы
        for (Pupil anyPupil : myClass) {
            anyPupil.study();
            anyPupil.read();
            anyPupil.write();
            anyPupil.read();
            System.out.println("-------------");
        }

        // вариант через коллекцию
//        ArrayList<Pupil> myClass = new ArrayList<Pupil>();
//        myClass.add(goodBoy1);
//        myClass.add(goodBoy2);
//        myClass.add(badBoy);
//        myClass.add(excellentBoy);
    }
}

