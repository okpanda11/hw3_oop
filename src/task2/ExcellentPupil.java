package task2;

public class ExcellentPupil extends Pupil {

    @Override
    protected void study() {
        System.out.println("EXCELLENT pupil study");
    }

    @Override
    protected void read() {
        System.out.println("EXCELLENT pupil read");
    }

    @Override
    protected void write() {
        System.out.println("EXCELLENT pupil write");
    }

    @Override
    protected void relax() {
        System.out.println("EXCELLENT pupil relax");
    }
}
