package task2;

public class GoodPupil extends Pupil{
    @Override
    protected void study() {
        System.out.println("GOOD pupil study");
    }

    @Override
    protected void read() {
        System.out.println("GOOD pupil read");
    }

    @Override
    protected void write() {
        System.out.println("GOOD pupil write");
    }

    @Override
    protected void relax() {
        System.out.println("GOOD pupil relax");
    }
}
