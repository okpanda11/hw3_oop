package task2;

public class Pupil {
    protected void study() {
        System.out.println("pupil study");
    }

    protected void read() {
        System.out.println("pupil read");
    }

    protected void write() {
        System.out.println("pupil write");
    }

    protected void relax() {
        System.out.println("pupil relax");
    }
}
