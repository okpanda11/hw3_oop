package task3;

public class Car extends Vehicle {

    protected Car(double coordinates, double price, int speed, int year) {
        super(coordinates, price, speed, year);
    }

    @Override
    protected void showInfo() {
        System.out.println("\n--- Car info: ---");
        super.showInfo();
    }


}
