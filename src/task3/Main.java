package task3;

public class Main {
    public static void main(String[] args) {
        Vehicle myPlane = new Plane(2525.25, 2000000, 300, 2005, 2500, 50);
        Vehicle myShip = new Ship(2121.21, 1500000, 90, 1990, 60, 2355);
        Vehicle myCar = new Car(1515.15, 50000, 200, 1995);

        // вызов переопределённых методов
        myPlane.showInfo();
        myShip.showInfo();
        myCar.showInfo();
    }
}
