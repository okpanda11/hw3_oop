package task3;

public class Plane extends Vehicle {
    // добавляем новые переменные для Plane
    private int maxHeight;
    private int maxPassengers;

    @Override
    protected void showInfo() {
        System.out.println("\n--- Plane info: ---");
        super.showInfo();
        System.out.println("maxHeight: " + maxHeight);
        System.out.println("maxPassengers: " + maxPassengers);
    }

    protected Plane(double coordinates, double price, int speed, int year, int maxHeight, int maxPassengers) {
        super(coordinates, price, speed, year);
        this.maxHeight = maxHeight;
        this.maxPassengers = maxPassengers;
    }

    protected void setMaxHeight(int maxHeight) {
        this.maxHeight = maxHeight;
    }

    protected int getMaxHeight() {
        return maxHeight;
    }

    protected void setMaxPassengers(int maxPassengers) {
        this.maxPassengers = maxPassengers;
    }

    protected int getMaxPassengers() {
        return maxPassengers;
    }
}

