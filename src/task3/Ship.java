package task3;

public class Ship extends Vehicle {
    // новые переменные для Ship
    private int maxPassengers;
    private int portNumber;

    @Override
    protected void showInfo() {
        System.out.println("\n--- Ship info: ---");
        super.showInfo();
        System.out.println("maxPassengers: " + maxPassengers);
        System.out.println("portNumber: " + portNumber);
    }

    protected Ship(double coordinates, double price, int speed, int year, int maxPassengers, int portNumber) {
        super(coordinates, price, speed, year);
        this.maxPassengers = maxPassengers;
        this.portNumber = portNumber;
    }

    protected void setMaxPassengers(int maxPassengers) {
        this.maxPassengers = maxPassengers;
    }

    protected int getMaxPassengers() {
        return maxPassengers;
    }

    protected void setPortNumber(int portNumber) {
        this.portNumber = portNumber;
    }

    protected int getPortNumber() {
        return portNumber;
    }
}
