package task3;

public class Vehicle {
    private double coordinates;
    private double price;
    private int speed;
    private int year;

    // method
    protected void showInfo() {
        System.out.println("coordinates: " + coordinates);
        System.out.println("price: " + price);
        System.out.println("maxSpeed: " + speed);
        System.out.println("was made in: " + year);
    }

    // constructor 4 parameters
    protected Vehicle(double coordinates, double price, int speed, int year) {
        this.coordinates = coordinates;
        this.price = price;
        this.speed = speed;
        this.year = year;
    }

    // getters & setters:
    protected void setCoordinates(double coordinates) {
        this.coordinates = coordinates;
    }

    protected double getCoordinates() {
        return coordinates;
    }

    protected void setPrice(double price) {
        this.price = price;
    }

    protected double getPrice() {
        return price;
    }

    protected void setSpeed(int speed) {
        this.speed = speed;
    }

    protected int getSpeed() {
        return speed;
    }

    protected void setYear(int year) {
        this.year = year;
    }

    protected int getYear() {
        return year;
    }

}

