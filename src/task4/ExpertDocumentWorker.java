package task4;

public class ExpertDocumentWorker extends ProDocumentWorker {

    @Override
    protected void saveDocument() {
        System.out.println("Документ сохранен в новом формате");
    }

    @Override
    protected void testDocument() {
        System.out.println("Этот метод будет скрыт через upCast до базового класса");
    }

}
