package task4;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите номер ключа: \n(1) - версия Pro\n(2) - версия Expert");
        int key = sc.nextInt(); //ключ доступа

        switch (key) {
            case 1 -> {
                DocumentWorker proDoc = new ProDocumentWorker();
                proDoc.editDocument();
                proDoc.saveDocument();
            }
            case 2 -> {
                DocumentWorker expertDoc = new ExpertDocumentWorker();
                expertDoc.editDocument();
                expertDoc.saveDocument();
            }
            default -> {
                DocumentWorker baseDoc = new DocumentWorker();
                baseDoc.openDocument();
                baseDoc.editDocument();
                baseDoc.saveDocument();
            }
        }
        sc.close();
    }
}
