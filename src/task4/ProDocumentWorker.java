package task4;

public class ProDocumentWorker extends DocumentWorker {

    @Override
    protected void editDocument() {
        System.out.println("Документ отредактирован");
    }

    @Override
    protected void saveDocument() {
        System.out.println("Документ сохранен в старом формате, сохранение в остальных форматах доступно в версии Эксперт");
    }

    protected void testDocument() {
        System.out.println("Этот метод будет скрыт через upCast до базового класса");
    }
}
